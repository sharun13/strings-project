const fullNameFinder = function (nameDetails) {

    if (nameDetails == undefined || typeof (nameDetails) !== 'object') {
        return [];
    }

    let fullName = '';

    if (nameDetails.first_name) {
        nameDetails.first_name = nameDetails.first_name.toLowerCase();
        let firstName = nameDetails.first_name.
            charAt(0)
            .toUpperCase() + nameDetails.first_name.slice(1);
        fullName = fullName + firstName + " ";
    }

    if (nameDetails.middle_name) {
        nameDetails.middle_name = nameDetails.middle_name.toLowerCase();
        let middleName = nameDetails.middle_name.
            charAt(0)
            .toUpperCase() + nameDetails.middle_name.slice(1);
        fullName = fullName + middleName + " ";
    }

    if (nameDetails.last_name) {
        nameDetails.last_name = nameDetails.last_name.toLowerCase();
        let lastName = nameDetails.last_name.
            charAt(0)
            .toUpperCase() + nameDetails.last_name.slice(1);
        fullName = fullName + lastName;
    }

    return fullName;
}

module.exports = fullNameFinder;

