const stringConcat = function(data) {

    if (data === undefined || typeof(data) != 'object') {
        return [];
    }

    let newString = '';

    data.map((word) => {
        newString = newString + word + " ";
    });

    return newString;

}

module.exports = stringConcat;