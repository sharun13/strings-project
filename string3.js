const monthFinder = function (date) {

    if (date == undefined) {
        return [];
    }

    let monthArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let splitDate = (date.split('/'));
    if(splitDate.length < 2) {
        return [];
    }
    
    let month = parseInt(splitDate[1] - 1);
    if(isNaN(month)) {
        return [];
    }

    if (month < 0 || month > 11) {
        return [];
    } else {
        let result = monthArray.at(month);
        return result;
    }

}

module.exports = monthFinder;