const findIP = function (string) {

    if (string == undefined || typeof (string) == 'number') {
        return [];
    }

    let splitIP = string.split('.');

    if (splitIP.length != 4) {
        return [];
    }

    let numberIP = splitIP.map((part) => {
        if (isNaN(part)) {
            return null;
        } else {
            return (parseInt(part));
        }
    });

    if (numberIP.includes(null)) {
        return [];
    } else {
        return numberIP;
    }

}

module.exports = findIP;