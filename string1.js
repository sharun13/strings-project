const extractNumber = function (string) {

    if (string == undefined) {
        return [];
    }

    let numbers = string.replace('$', '')
        .replace(',', '');

    if (isNaN(numbers)) {
        return 0;
    } else {
        numbers = parseFloat(numbers);
        return numbers;
    }

}

module.exports = extractNumber;